#include <QObject>

#include <cstring>
#include <gcrypt.h>
#include "egg.h"

#include "Utils.h"


// taken from libsecret/secret-session.c
// LICENSE: gpl I think
QByteArray pkcs7_pad_bytes(QByteArray inp) {
	int n_padded;
	int length = inp.length();

	/* Pad the secret */
	n_padded = ((length + 16) / 16) * 16;
	Q_ASSERT (length < n_padded);
	Q_ASSERT (n_padded > 0);
	int n_pad = n_padded - length;
	Q_ASSERT (n_pad > 0 && n_pad <= 16);
	QByteArray padded = inp;
	for (int i = 0; i < n_pad; i++) {
		padded.append(n_pad);
	}
	return padded;
}


#include "Session.h"
const QString _Session::interface = "org.freedesktop.Secret.Session";

_Session::_Session(QObject *parent, ALGORITHM_TYPE type) 
	: DBusContext(parent)
	, type(type)
	{
}

_Session::_Session(QObject *parent, ALGORITHM_TYPE type, QByteArray &peerData) 
	: DBusContext (parent)
	, path(generatePath())
	, type(type)
	, keys(genKey())
	, publickey(keys.publicKey)
	, sharedKey(handshake(keys, peerData)) {
}

QDBusObjectPath _Session::generatePath(void) {
	return QDBusObjectPath("/org/freedesktop/secrets/session/" + GetRandomString(12));
}


QByteArray convert(gcry_mpi_t number) {
		unsigned char *buffer; // Do I have to free this?
		size_t n_buffer;

		gcry_mpi_aprint(GCRYMPI_FMT_USG, &buffer, &n_buffer, number); // TODO: check return val
		return QByteArray(reinterpret_cast<char *>(buffer), n_buffer);
}

Keys _Session::genKey(void) {
	debugline();
		egg_libgcrypt_initialize();
		gcry_mpi_t prime, base, my_publickey, my_privatekey;
		egg_dh_default_params("ietf-ike-grp-modp-1024", &prime, &base); // TODO: check return val
		egg_dh_gen_pair(prime, base, 0, &my_publickey, &my_privatekey); // TODO: check return val

		return {convert(my_publickey), convert(my_privatekey), convert(prime)};
}

QByteArray _Session::handshake(Keys &key, QByteArray &peer_data) {
	debugline();
		gcry_mpi_t peer, my_privatekey, prime;
		gcry_mpi_scan(&peer, GCRYMPI_FMT_USG, peer_data, peer_data.length(), NULL);
		gcry_mpi_scan(&my_privatekey, GCRYMPI_FMT_USG, key.privateKey, key.privateKey.length(), NULL);
		gcry_mpi_scan(&prime, GCRYMPI_FMT_USG, key.prime, key.prime.length(), NULL);
		void *ikm;
		size_t n_ikm;



		ikm = egg_dh_gen_secret(peer, my_privatekey, prime, &n_ikm);

		size_t n_hashed = 16;
		char hashed[n_hashed];
		egg_hkdf_perform ("sha256", ikm, n_ikm, NULL, 0, NULL, 0, hashed, n_hashed);

		debugline();
		return QByteArray(hashed, n_hashed);
}

EncryptedMessage _Session::encrypt(QString msg) {
	debugline();
	qWarning() << this;
	switch (type) {
		case PLAIN:
			return {QByteArray(), QByteArray(msg.toLocal8Bit())};
		case DH: {
				debugline();
				// fancy encryption
				QByteArray iv(16, 0); // null array of len 16
				QByteArray encryptedMessage;
				gcry_cipher_hd_t cih;

				qWarning() << "RETURN: " << gcry_cipher_open (&cih, GCRY_CIPHER_AES, GCRY_CIPHER_MODE_CBC, 0);

				gcry_create_nonce(iv.data(), iv.length()); // TODO: check return val
				qWarning() << "RETURN: " << gcry_cipher_setiv(cih, iv.data(), iv.length()); // TODO: chheck return val



				qWarning() << "RETURN: " << gcry_cipher_setkey(cih, sharedKey.data(), sharedKey.length());

				QByteArray padded = pkcs7_pad_bytes(msg.toLocal8Bit());
				encryptedMessage = QByteArray(padded.length(), 0); // Zero array of padded length
				for (int pos = 0; pos < padded.length(); pos += 16) {
					qWarning() << gcry_cipher_encrypt(cih, encryptedMessage.data() + pos, 16, padded.data() + pos, 16); // TODO: check return val
				}
				gcry_cipher_close(cih);
				return {iv, encryptedMessage};
			}
		default:
			throw;
	}
}


void _Session::Close(void) {
	//delete this;
}
