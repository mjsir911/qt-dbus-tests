#ifndef PROMPT_H
#define PROMPT_H
#include <QtDBus/QtDBus>
#include "Types.h"

#include "Service.h"

#include "DBus.h"

class _Prompt : public DBusContext  {
	Q_OBJECT

private:
	QDBusObjectPath generatePath(void);
	QDBusVariant something;
	const QDBusObjectPath path;
	const QString interface = "org.freedesktop.Secret.Prompt";

public:
	const QDBusObjectPath getPath() const override {
		return path;
	}
	const QString getInterface() const override {
		return interface;
	}

	_Prompt(_Service *, QDBusVariant);
	~_Prompt();

public Q_SLOTS: // METHODS
    void Dismiss();
    void Prompt(const QString &window_id);
Q_SIGNALS: // SIGNALS
    void Completed(bool dismissed, const QDBusVariant &result);
};
#endif
