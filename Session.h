#ifndef SESSION_H
#define SESSION_H

#include <QtDBus/QtDBus>
#include "Types.h"

#include "DBus.h"

struct EncryptedMessage {
	QByteArray nonce;
	QByteArray payload;
};


struct Keys {
	QByteArray publicKey;
	QByteArray privateKey;
	QByteArray prime;
};

class _Session : public DBusContext {
	Q_OBJECT

	Keys keys;
	QByteArray sharedKey;
	QByteArray handshake(Keys &, QByteArray &);
	Keys genKey(void);
	QDBusObjectPath generatePath(void);
	const QDBusObjectPath path;
	static const QString interface;


public:
	PATH_INTERFACE_GETTERS;
	const QByteArray publickey;
	enum ALGORITHM_TYPE {
		PLAIN,
		DH
	};
	EncryptedMessage encrypt(QString);

private:
	const ALGORITHM_TYPE type;

public:
// CONSTRUCTORS
_Session(QObject *, ALGORITHM_TYPE);
_Session(QObject *, ALGORITHM_TYPE, QByteArray &);

public Q_SLOTS: // METHODS
    void Close();
Q_SIGNALS: // SIGNALS
};
#endif
