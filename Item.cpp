#include <QtDBus/QtDBus>
#include <QThread>

#include "Item.h"
#include "ItemAdaptor.h"
#include "ItemInterface.h"

#include "Utils.h"

#include "core/Entry.h"
#include "core/Uuid.h"

const QString _Item::interface = "org.freedesktop.Secret.Item";

#include "Service.h"
_Item::_Item(_Collection *parent, const QString &label, const Entry *kpEntry)
	: kpEntry(kpEntry)
	, label(label)
	, attributes(StringMap())
	, path(QDBusObjectPath(parent->getPath().path() + "/" + kpEntry->uuid().toHex()))
	, DBusContext(parent)
{
	initDBus<_Item, ItemAdaptor, OrgFreedesktopSecretItemInterface>();

	
	if (!kpEntry->url().isEmpty()) {
		attributes = network_protocol_entry();
	} else {
		attributes["xdg:schema"] = "org.freedesktop.Secret.Generic";
		attributes["Username"] = kpEntry->username();
	}
}

StringMap _Item::network_protocol_entry(void) {
	StringMap ret;
	ret["xdg:schema"] = "org.gnome.keyring.NetworkPassword";
	ret["user"] = kpEntry->username();
	// TODO: do magic regex on url for:
	// 	object(path)
	// 	protocol(https esq)
	// 	port
	// 	server
	return ret;
}

_Item::_Item(_Collection *parent, const Entry *kpEntry) : _Item(parent, kpEntry->title(), kpEntry) {}

_Item::~_Item() {
	connection().unregisterObject(path.path());
}

QDBusObjectPath _Item::Delete() {
	throw NotImplementedException();
}

SecretStruct _Item::GetSecret(const QDBusObjectPath &session) {
	// TODO: look up password in database
	QString message = kpEntry->password();
  #define PASSWORD_MIME "text/plain"
	
	if (!_Service::getActive()->encryptionDict.contains(session)) {
		qWarning() << _Service::getActive()->encryptionDict.values();
		throw "hi";
	}
	EncryptedMessage msg = _Service::getActive()->encryptionDict[session]->encrypt(message);
	return {session, msg.nonce, msg.payload, PASSWORD_MIME};
}

void _Item::SetSecret(SecretStruct secret) {
	throw NotImplementedException();
}
