#include "DBus.h"

#include <QtDBus/QtDBus>

#include "Utils.h"

DBusContext::DBusContext(QObject *parent) : QObject(parent) { }

QDBusConnection DBusContext::connection() const {
	if (calledFromDBus()) {
		return connection();
	} else {
		return QDBusConnection::sessionBus();
	}
}

void DBusContext::PropertiesChanged(const QString& propertyName) {
	// https://randomguy3.wordpress.com/2010/09/07/the-magic-of-qtdbus-and-the-propertychanged-signal/
	QDBusMessage signal = QDBusMessage::createSignal(
			getPath().path(),
			"org.freedesktop.DBus.Properties",
			"PropertiesChanged");
	signal << getInterface() << QVariantMap({{propertyName, property(propertyName.toLocal8Bit())}}) << QStringList();
	connection().send(signal);
}
