class _Service;
#ifndef SERVICE_H
#define SERVICE_H

#include <QtDBus/QtDBus>

#include "Types.h"

#include "Item.h"

#include "Utils.h"

#include "Collection.h"

#include "DBus.h"

#include "Session.h"

class _Service : public DBusContext {
	Q_OBJECT

friend _Item;
private:
	const static QDBusObjectPath path;
	const static QString interface;

	_Collection *getDefault(void);

	QSet<_Collection *> collections;

	QList<QDBusObjectPath> getCollectionPaths(void);

public:
	QMap<QDBusObjectPath, _Session *> encryptionDict;
	PATH_INTERFACE_GETTERS

public:
	// CONSTRUCTORS
	_Service(QObject *, QDBusConnection &);
	static _Service *getActive(void);

	// MUTATORS
	void addCollection(_Collection *);
	void removeCollection(_Collection *);


// PROPERTIES
public:
	Q_PROPERTY(QList<QDBusObjectPath> Collections READ getCollectionPaths);


public Q_SLOTS: // METHODS
    QDBusObjectPath ChangeLock(const QDBusObjectPath &collection);
    QDBusObjectPath CreateCollection(VariantMap properties, const QString &alias, QDBusObjectPath &prompt);
    SecretsDict GetSecrets(const QList<QDBusObjectPath> &items, const QDBusObjectPath &session);
    QList<QDBusObjectPath> Lock(const QList<QDBusObjectPath> &objects, QDBusObjectPath &Prompt);
    void LockService();
    QDBusVariant OpenSession(const QString &algorithm, const QDBusVariant &input, QDBusObjectPath &result);
    QDBusObjectPath ReadAlias(const QString &name);
    QList<QDBusObjectPath> SearchItems(StringMap attributes, QList<QDBusObjectPath> &locked);
    void SetAlias(const QString &name, const QDBusObjectPath &collection);
    QList<QDBusObjectPath> Unlock(const QList<QDBusObjectPath> &objects, QDBusObjectPath &prompt);
Q_SIGNALS: // SIGNALS
    void CollectionChanged(const QDBusObjectPath &collection);
    void CollectionCreated(const QDBusObjectPath &collection);
    void CollectionDeleted(const QDBusObjectPath &collection);
};
#endif
