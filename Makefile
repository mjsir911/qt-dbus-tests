.SUFFIXES:
# CXXFLAGS := $(shell pkg-config zlib Qt5DBus Qt5Gui Qt5Core Qt5Widgets Qt5Network Qt5X11Extras glib-2.0 libsodium --cflags) -fPIC -ggdb -g3 $(INCPATH)
# LDFLAGS  := $(shell pkg-config zlib Qt5DBus Qt5Gui Qt5Core Qt5Widgets Qt5Network Qt5X11Extras glib-2.0 libsodium --libs) -largon2 -lX11 $(shell /usr/bin/x86_64-pc-linux-gnu-libgcrypt-config --libs) $(wildcard /home/msirabella/Documents/projects/featurefork/keepassxc/src/*.a) 
INCPATH  := -I /home/msirabella/Documents/projects/featurefork/keepassxc/src
PKGS = Qt5Core Qt5Widgets Qt5Core Qt5Widgets Qt5Network Qt5Gui Qt5DBus glib-2.0 zlib
CXXFLAGS := $(shell pkg-config $(PKGS) --cflags) -fPIC $(INCPATH)
LDFLAGS := $(shell /usr/bin/x86_64-pc-linux-gnu-libgcrypt-config --libs) $(shell pkg-config $(PKGS) glib-2.0 zlib --libs) -largon2


SERVICE = org.freedesktop.Secret
INTERFACES = Service Collection Item Session Prompt
 
.PRECIOUS: %.o


TARGET = server

EGGFILES := $(wildcard egg/*.o)
$(TARGET): Service.dbus.o main.cpp Types.cpp Prompt.dbus.o Collection.dbus.o Item.dbus.o  $(EGGFILES) DBus.cpp Session.dbus.o /home/msirabella/Documents/projects/featurefork/keepassxc/src/libkeepassx_core.a /home/msirabella/Documents/projects/featurefork/keepassxc/src/libautotype.a /home/msirabella/Documents/projects/featurefork/keepassxc/src/libzxcvbn.a
	g++ $^ -o $@ $(CXXFLAGS) $(LDFLAGS)

%Interface.cpp %Interface.h: $(SERVICE)s.xml %.h Types.h
	qdbusxml2cpp $< $(addprefix -i,$(filter %.h %.cpp,$^)) -l _$* -p $*Interface $(SERVICE).$*

%Adaptor.cpp %Adaptor.h: $(SERVICE)s.xml %.h Types.h
	qdbusxml2cpp $< $(addprefix -i,$(filter %.h %.cpp,$^)) -l _$* -a $*Adaptor $(SERVICE).$*

%.moc: %.cpp %.h # For some reason the order here determines an interemediate file being removed. Ah well, better cpp get removed than .h
	moc -f $(word 1,$^) $(DEFINES) $(INCPATH) $(word 2,$^) -o $@

%.o: %.moc
	$(COMPILE.cpp) -xc++ $(OUTPUT_OPTION) $<

# %.o: %.cpp
	# $(COMPILE.cpp) $(OUTPUT_OPTION) $<

%.dbus.o: %.o %Adaptor.o %Interface.o
	$(LD) $(OUTPUT_OPTION) -r $^ 

clean:
	${RM} *.moc *Adaptor* *Interface* *.o $(TARGET)
