#include "Service.h"
#include "ServiceAdaptor.h"
#include "ServiceInterface.h"

#include "Prompt.h"

#include "Utils.h"

#include "Collection.h"

#include "core/Config.h"

const QDBusObjectPath _Service::path = QDBusObjectPath("/org/freedesktop/secrets");
const QString _Service::interface = "org.freedesktop.Secret.Service";

_Collection *_Service::getDefault(void) {
	return _Collection::getAlias("default");
}

_Service::_Service(QObject *parent, QDBusConnection &bus) : DBusContext(parent) {
	initDBus<_Service, ServiceAdaptor, OrgFreedesktopSecretServiceInterface>();

	for (QFileInfo past_databse: config()->get("LastDatabases", QVariant()).toStringList()) {
		collections.insert(new _Collection(this, past_databse));
		PropertiesChanged("Collections");
	}

}


QDBusObjectPath _Service::ChangeLock(const QDBusObjectPath &collection) {
	debugline();
	throw NotImplementedException();
}

QDBusObjectPath _Service::CreateCollection(VariantMap properties, const QString &alias, QDBusObjectPath &prompt) {
	debugline();
	//QString file_path = properties[LABEL_LABEL].variant().value<QString>();
	//prompt = *(new _Prompt(this, QDBusVariant(QVariant::fromValue((new _Collection(this, properties))->getPath()))));
	//prompt = new _Prompt(this, connection(), QDBusVariant(QVariant::fromValue(QDBusObjectPath()));
	auto collection = new _Collection(this, properties);
	addCollection(collection);
	
	prompt = QDBusObjectPath("/");
	return collection->getPath();
	//throw NotImplementedException();
}


SecretsDict _Service::GetSecrets(const QList<QDBusObjectPath> &items, const QDBusObjectPath &session) {
	debugline();
	// TODO: iterate over collections doing searchitems
}

QList<QDBusObjectPath> _Service::Lock(const QList<QDBusObjectPath> &objects, QDBusObjectPath &Prompt) {
	debugline();
	//Prompt = *(new _Prompt(this, QDBusVariant(QVariant::fromValue(objects))));
	Prompt = QDBusObjectPath("/");
	return objects;
	//throw NotImplementedException();
}

void _Service::LockService() {
	debugline();
	throw NotImplementedException();
}


// EGG_SECURE_DEFINE_GLIB_GLOBALS ();

QDBusVariant _Service::OpenSession(const QString &algorithm, const QDBusVariant &input, QDBusObjectPath &sessionPath) {
	_Session *a;
	if (algorithm == "plain") {
		a = new _Session(this, _Session::PLAIN);
	} else if (algorithm == "dh-ietf1024-sha256-aes128-cbc-pkcs7") {
		QByteArray peer_data = input.variant().value<QByteArray>();

		a = new _Session(this, _Session::DH, peer_data);
	} else {
		sendErrorReply(QDBusError::NotSupported, "um, no"); 
		throw;
	}
	sessionPath = a->getPath();;

	qWarning() << sessionPath.path();
	debugline();
	debugline();
	debugline();
	debugline();
	debugline();
	debugline();
	encryptionDict[sessionPath] = a;
	return QDBusVariant(a->publickey);
}

QDBusObjectPath _Service::ReadAlias(const QString &name) {
	debugline();
	_Collection *collection;
	if (! (collection = _Collection::getAlias(name))) {
		// TODO: fail
		return QDBusObjectPath("/");
	}
	return collection->getPath();
}

QList<QDBusObjectPath> _Service::SearchItems(StringMap attributes, QList<QDBusObjectPath> &locked) {
	debugline();
#define database_locked false
	if (database_locked) { // If keepassxc database is locked
		throw NotImplementedException();
		return QList<QDBusObjectPath>();
	} else {
		locked = QList<QDBusObjectPath>();
		// TODO: search database
		return getDefault()->SearchItems(attributes);
	}
}

void _Service::SetAlias(const QString &name, const QDBusObjectPath &collection_path) {
	_Collection *collection = _Collection::getExisting(collection_path);
	collection->addAlias(name);
	//debugline();
	//throw NotImplementedException();
}

QList<QDBusObjectPath> _Service::Unlock(const QList<QDBusObjectPath> &objects, QDBusObjectPath &prompt) {
	debugline();
	prompt = *(new _Prompt(this, QDBusVariant(QVariant::fromValue(objects))));
	return QList<QDBusObjectPath>(); // If we ever get this far, there are probably no unlocked objects
}

QList<QDBusObjectPath> _Service::getCollectionPaths(void) {
	debugline();
	QList<QDBusObjectPath> a;
	foreach (_Collection *value, collections) {
		debugline();
		std::cerr << value->getPath().path().toStdString() << "\n";
		a.append(*value);
	}
	for (auto value : a) {
		debugline();
		qWarning() << value.path();
	}
	return a;
}

void _Service::addCollection(_Collection *collection) {
	debugline();
	collections.insert(collection);
	PropertiesChanged("Collections");
}
void _Service::removeCollection(_Collection *collection) {
	debugline();
	collections.remove(collection);
	PropertiesChanged("Collections");
}

_Service *_Service::getActive(void) {
	return qobject_cast<_Service *>(QDBusConnection::sessionBus().objectRegisteredAt(path.path()));
}
