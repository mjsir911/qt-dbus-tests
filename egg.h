#ifndef EGG_H
#define EGG_H

#include <glib.h>

#include <gcrypt.h>

extern "C" {    // another way
#include "egg/egg-libgcrypt.h"
#include "egg/egg-dh.h"
#include "egg/egg-secure-memory.h"
#include "egg/egg-hkdf.h"
}
// EGG_SECURE_DEFINE_GLIB_GLOBALS ();
#endif
