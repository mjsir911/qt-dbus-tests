#ifndef DBUS_H
#define DBUS_H

#include <QObject>
#include <QtDBus/QtDBus>

#include "Utils.h"

#define PATH_INTERFACE_GETTERS const QDBusObjectPath getPath() const override { return path; } const QString getInterface() const override { return interface; }

class DBusContext : public QObject, protected QDBusContext {
public:
	// constructors
	DBusContext(QObject *);

	QDBusConnection connection() const;
	void PropertiesChanged(const QString &);

	// Getters
	virtual const QDBusObjectPath getPath(void) const = 0;
	virtual const QString getInterface(void) const = 0;

	template <class cls, class Adaptor, class Interface>
	void initDBus(void);

	// Type Coersions
	inline operator QDBusObjectPath() {return getPath();}
};

template <class cls, class Adaptor, class Interface>
void DBusContext::initDBus() {
	cls *self = static_cast<cls *>(this);

	new Adaptor(self);
	new Interface(getInterface(), getPath().path(), connection(), self);

	connection().registerService(getInterface());
	connection().registerObject(getPath().path(), self);
}

#endif
