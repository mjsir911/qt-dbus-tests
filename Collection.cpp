#include <QtDBus/QtDBus>
#include "Types.h"

#include "Utils.h"

#include "Collection.h"
#include "CollectionAdaptor.h"
#include "CollectionInterface.h"

#include "Service.h"

#include "Item.h"

#include "core/Database.h"
#include "core/Group.h"

QMap<QString, _Collection *> _Collection::aliases;
const QString _Collection::interface = "org.freedesktop.Secret.Collection";

_Collection::_Collection(_Service *parent, QFileInfo file)
	: DBusContext(parent)
	, file(file)
	, path(QDBusObjectPath(COLLECTIONS_PATH + property("Label").value<QString>()))
	, kpDatabase(nullptr)
{
	qDebug() << file.path();
	Q_ASSERT_X(file.exists(), __PRETTY_FUNCTION__, "File does not exist");
	//kpDatabase = Database::unlockFromStdin("/home/msirabella/passwords.kdbx");
	//qWarning() << file.baseName();
	initDBus<_Collection, CollectionAdaptor, OrgFreedesktopSecretCollectionInterface>();
}

_Collection::_Collection(_Service *parent, VariantMap attributes) : _Collection(parent, attributes[LABEL_LABEL].variant().value<QString>()) {}

_Collection::~_Collection() {
	qobject_cast<_Service *>(parent())->removeCollection(this);
	connection().unregisterObject(path.path());
}

#include "keys/CompositeKey.h"
#include "keys/PasswordKey.h"

void _Collection::unlockDatabase(const QString &password) {
	CompositeKey compositeKey;
	PasswordKey passwordKey(password);
	compositeKey.addKey(passwordKey);

	// TODO: check that was already unlocked, kpDatabase == nullptr
	kpDatabase = Database::openDatabaseFile(file.filePath(), compositeKey);
	qWarning() << kpDatabase;
}

void _Collection::lockDatabase() {
	// important
	delete kpDatabase;
	kpDatabase = nullptr;
}

void _Collection::setDefault(void) {
	addAlias(DEFAULT);
}

QDBusObjectPath _Collection::CreateItem(VariantMap properties, SecretStruct secret, bool replace, QDBusObjectPath &prompt) {
	throw NotImplementedException();

	QString label = properties[ITEM_LABEL].variant().value<QString>();

	if (replace || !items.contains(label)) {
		delete items[label]; // just in case
		//items[label] = new _Item(this, properties, secret);
	}
	prompt = QDBusObjectPath("/");
	PropertiesChanged("Items");
	return items[label]->getPath();
}

QDBusObjectPath _Collection::Delete() {
	_Service::getActive()->removeCollection(this);
	return QDBusObjectPath("/");
}

QList<QDBusObjectPath> _Collection::SearchItems(StringMap attributes) {
	throw NotImplementedException();
}


void _Collection::addAlias(const QString &alias) {
	connection().unregisterObject(ALIAS_PATH + alias);
	connection().registerObject(ALIAS_PATH + alias, this);
	aliases[alias] = this;
}

_Collection *_Collection::getAlias(const QString &alias) {
	return aliases[alias];
}

_Collection *_Collection::getExisting(const QDBusObjectPath &path) {
	_Collection *ret = qobject_cast<_Collection *>(QDBusConnection::sessionBus().objectRegisteredAt(path.path()));
	if (!ret) {
		 // TODO: fail
	}
	qWarning() << ret;
	return ret;
}

QList<QDBusObjectPath> _Collection::getItems(void) {
	if (property("Locked").value<bool>()) {
		return {};
	}
	qWarning() << kpDatabase;

	QList<QDBusObjectPath> ret;
	for (auto entry: kpDatabase->rootGroup()->entriesRecursive()) {
		QString label = entry->title();
		QString key = entry->uuid().toBase64();
		if (!items.contains(key)) {
			//items[key] = new _Item(this, (!label.isEmpty() ? label : GetRandomString()), entry); // enable this for random rather than empty labels
			items[key] = new _Item(this, entry);
		}
		if (items[key]->getPath().path().isEmpty()) {
			qCritical() << key;
		}
		ret.append(items[key]->getPath());
	}

	return ret;
}
