class _Item;
#ifndef ITEM_H
#define ITEM_H

#include <QtDBus/QtDBus>
#include "Types.h"

#include "Collection.h"
#include "Session.h"

#include "DBus.h"

#include "core/Entry.h"

#define ITEM_LABEL _Item::interface + ".Label"
#define ITEM_ATTRIBUTES _Item::interface + ".Attributes"

class _Item : public DBusContext {
	Q_OBJECT

private:
	StringMap attributes;
	QString label;

	QDBusObjectPath path;

	const Entry *kpEntry;

	StringMap network_protocol_entry(void);


public:
	PATH_INTERFACE_GETTERS
	const static QString interface;

	// CONSTRUCTORS
	_Item(_Collection *, const Entry *);
	_Item(_Collection *, const QString &, const Entry *);
	// _Item(const _Item&);
	// _Item& operator=(const _Item&);

	// DESTRUCTORS
	~_Item();


public: // PROPERTIES
	Q_PROPERTY(QString Label MEMBER label);
	Q_PROPERTY(StringMap Attributes MEMBER attributes);

public Q_SLOTS: // METHODS
    QDBusObjectPath Delete();
    SecretStruct GetSecret(const QDBusObjectPath &session);
    void SetSecret(SecretStruct secret);
Q_SIGNALS: // SIGNALS
};
#endif
