#include <QtDBus/QtDBus>

#include "Types.h"

#include <iostream>

#include "Service.h"

#include "Prompt.h"
#include "Collection.h"

#include <QDBusMetaType>

#include "egg.h"
EGG_SECURE_DEFINE_GLIB_GLOBALS ();

int main(int ac, char **av) {
	registerMetaTypes();


	QCoreApplication a(ac, av);

	QDBusConnection dbus = QDBusConnection::sessionBus();
	dbus.registerService("org.freedesktop.secrets");

	_Service secret(&a, dbus);

	//_Collection only(&secret, "marco");
	//only.setDefault();

	//_Collection other(&secret, dbus, "hi");

	//_Prompt hi(&a, dbus);

	a.exec();

}
