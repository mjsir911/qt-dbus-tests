class _Collection;
#ifndef COLLECTION_H
#define COLLECTION_H

#include <QtDBus/QtDBus>
#include "Types.h"

#define ROOT_PATH "/org/freedesktop/secrets/"

#define ALIAS_PATH ROOT_PATH "aliases/"
#define DEFAULT "default"
#define DEFAULT_COLLECTION_PATH ALIAS_PATH DEFAULT

#define COLLECTIONS_PATH ROOT_PATH "collection/"

#define LABEL_LABEL (_Collection::interface + ".Label")

#include "Service.h"

#include "Item.h"

#include "DBus.h"

#include "core/Database.h"

class _Collection : public DBusContext {
	Q_OBJECT

private:
	static QMap<QString, _Collection *> aliases;

	QMap<QString, _Item *> items;

	const QFileInfo file;
	const QDBusObjectPath path;
	static const QString interface;

	Database *kpDatabase;

public:
	PATH_INTERFACE_GETTERS

	// Constructors
	_Collection(_Service *, QFileInfo);
	_Collection(_Service *, VariantMap);
	static _Collection *getExisting(const QDBusObjectPath &);

	// Deconstructor
	~_Collection();

	// Aliases
	void addAlias(const QString&);
	void setDefault(void);
	static _Collection *getAlias(const QString &alias);

	void unlockDatabase(const QString &);
	void lockDatabase();


// PROPERTIES
public:
	Q_PROPERTY(QString Label READ getLabel);
	Q_PROPERTY(bool Locked READ isLocked);
	Q_PROPERTY(qulonglong Created READ getCreated);
	Q_PROPERTY(QList<QDBusObjectPath> Items READ getItems NOTIFY ItemsChanged);

private:
	QList<QDBusObjectPath> getItems(void);
	inline qulonglong getCreated(void) {return file.created().toSecsSinceEpoch();};
	inline bool isLocked(void) {return kpDatabase == nullptr;};
	inline QString getLabel(void) {return file.baseName();};

signals:
	void ItemsChanged(void);

public Q_SLOTS: // METHODS
    QDBusObjectPath CreateItem(VariantMap properties, SecretStruct secret, bool replace, QDBusObjectPath &prompt);
    QDBusObjectPath Delete();
    QList<QDBusObjectPath> SearchItems(StringMap attributes);
Q_SIGNALS: // SIGNALS
    void ItemChanged(const QDBusObjectPath &item);
    void ItemCreated(const QDBusObjectPath &item);
    void ItemDeleted(const QDBusObjectPath &item);
};
#endif
