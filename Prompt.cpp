#include <QtDBus/QtDBus>
#include <QThread>

#include "Prompt.h"
#include "PromptAdaptor.h"
#include "PromptInterface.h"


#include "Utils.h"

#include "Service.h"

#include "DBus.h"

_Prompt::_Prompt(_Service *parent, QDBusVariant something) : path(_Prompt::generatePath()), DBusContext(parent), something(something) {
	debugline();
	initDBus<_Prompt, PromptAdaptor, OrgFreedesktopSecretPromptInterface>();

}
_Prompt::~_Prompt() {
	connection().unregisterObject(path.path());
}


QDBusObjectPath _Prompt::generatePath(void) {
	return QDBusObjectPath("/org/freedesktop/secrets/prompts/" + GetRandomString(12));
}


void _Prompt::Dismiss() {
	debugline();
	throw NotImplementedException();
	delete this;
}

#include "cli/Utils.h"
#include "Collection.h"
void _Prompt::Prompt(const QString &window_id) {
	debugline();

	QTextStream(stdout) << "Insert password to unlock: ";
	QString password = Utils::getPassword();
	auto somethings = something.variant().value<QList<QDBusObjectPath>>();
	
	QList<QDBusObjectPath> succeeded;
	for (QDBusObjectPath path: somethings) {
		_Collection *collection = _Collection::getExisting(path);
		collection->unlockDatabase(password);
		collection->PropertiesChanged("Items");
		if (!collection->property("Locked").value<bool>()) {
			succeeded.append(collection->getPath());
		}

	}
	// TODO: prompt master password
	// qWarning() << "window_id: " << window_id;
	emit Completed(true, QDBusVariant(QVariant::fromValue(succeeded)));
	QThread::usleep(1); // Holy shit why is this needed
	delete this;
}
